﻿


## Object detection : Graphical object ( table, figure, natural image, logo, signature)
1. [  IIIT-AR-13K: A New Dataset for Graphical Object Detection in Documents ](https://arxiv.org/abs/2008.02569)
2. [Graphical object detection in document images](https://arxiv.org/pdf/2008.10843v1.pdf)
3. Kavasidis, I., Pino, C., Palazzo, S., Rundo, F., Giordano, D., Messina, P., Spampinato, C.: A saliency-based convolutional neural network for table and chart detection in digitized documents. In: International Conference on Image Analysis and  Processing (2019)
4. Li, M., Cui, L., Huang, S., Wei, F., Zhou, M., Li, Z.: TableBank: Table benchmark  for image-based table detection and recognition. In: ICDAR (2019)  
5. Li, Y., Yan, Q., Huang, Y., Gao, L., Tang, Z.: A GAN-based feature generator for  table detection. In: ICDAR (2019)
6. Schreiber, S., Agne, S., Wolf, I., Dengel, A., Ahmed, S.: Deepdesrt: Deep learning  
for detection and structure recognition of tables in document images. In: ICDAR  
(2017)
7. Abdulla, W.: Mask R-CNN for object detection and instance segmentation on  
Keras and Tensorflow. GitHub repository (2017)
8. Chi, Z., Huang, H., Xu, H.D., Yu, H., Yin, W., Mao, X.L.: Complicated table  structure recognition. arXiv (2019)
9. Saha, R., Mondal, A., Jawahar, C.V.: Graphical object detection in document  
images. In: ICDAR (2019)
## Segmentation
### Document layout analysis
1. [Fast CNN-based document layout analysis](https://openaccess.thecvf.com/content_ICCV_2017_workshops/papers/w18/Oliveira_Fast_CNN-Based_Document_ICCV_2017_paper.pdf)
2. [Visual Detection with Context for Document Layout Analysis](https://www.aclweb.org/anthology/D19-1348.pdf)
3. [R. Girshick, “Fast R-CNN,” in ICCV, 2015](https://arxiv.org/abs/1504.08083) 
4.  [K. He, X. Zhang, S. Ren, and J. Sun, “Deep residual learning for image recognition,” in CVPR, 2016](https://arxiv.org/abs/1512.03385)
5. [S. Schreiber, S. Agne, I. Wolf, A. Dengel, and S. Ahmed,  “Deepdesrt: Deep learning for detection and structure recognition of tables in document images,” in ICDAR, 2017]()
6. [Table Structure Recognition using Top-Down and Bottom-Up Cues Sachin Raja, Ajoy Mondal, and C V Jawahar](https://www.ecva.net/papers/eccv_2020/papers_ECCV/papers/123730069.pdf)
7.  L. Kang, J. Kumar, P. Ye, Y. Li, and D. Doermann. Convolutional neural networks for document image classification. In 2014 22nd International Conference on Pattern Recognition, pages 3168–3172, Aug 2014.
8. A. W. Harley, A. Ufkes, and K. G. Derpanis. Evaluation of deep convolutional nets for document image classification and retrieval. In Proceedings of the 2015 13th International Conference on Document Analysis and Recognition (ICDAR), ICDAR ’15, pages 991–995, Washington, DC, USA, 2015. IEEE Computer Society.
9. [A generic deep-learning approach for document segmentation](https://arxiv.org/pdf/1804.10371.pdf)
