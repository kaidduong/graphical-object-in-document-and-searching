﻿


## 1.Layout:
### 1.1. Purpose:
- Extracting feature the graphical object in document images(ex: table, natural images, figure, logo,etc) to build a images database for ***image search engine***.

### 1.2. Image segmentation
#### 1.2.1 Introduction
- An image is a way of transferring information, and the image contains lots of useful information. Understanding the image and extracting information from the image to accomplish some works is an important area of application in digital image technology, and the first step in understanding the image is the image segmentation. In practice, it is often not interested in all parts of the image, but only for some certain areas which have the same characteristics. Image segmentation is one of the hotspots in image processing and computer vision. It is also an important basis for image recognition. It is based on certain criteria to divide an input image into a number of the same nature of the category in order to extract the area which people are interested in. And it is the basis for image analysis and understanding of image feature extraction and recognition.
####  1.2.2.  The Different Types of Image Segmentation
- We can broadly divide image segmentation techniques into two types. Consider the below images:

![semantic and instance segmentation](https://cdn.analyticsvidhya.com/wp-content/uploads/2019/03/Screenshot-from-2019-03-28-11-45-55.png)

Can you identify the difference between these two? Both the images are using image segmentation to identify and locate the people present.

-   In image 1, every pixel belongs to a particular class (either background or person). Also, all the pixels belonging to a particular class are represented by the same color (background as black and person as pink). This is an example of **semantic segmentation**-   Image 2 has also assigned a particular class to each pixel of the image. However, different objects of the same class have different colors (Person 1 as red, Person 2 as green, background as black, etc.). This is an example of **instance segmentation**

![semantic and instance segmentation](https://cdn.analyticsvidhya.com/wp-content/uploads/2019/03/Screenshot-from-2019-03-28-12-08-09.png)

#### 1.2.3  Image segmentation algorithms

| Algorithm   | Description   | Advantages  | Limitations |
| ------------- |:---------------------------------------------:| ---------------------:|--------------:|
| Region-Based Segmentation  | Separates the objects into different regions based on some threshold value(s).x 1   |Simple calculations; Fast operation speed; When the object and background have high contrast, this method performs really well                      | When there is no significant grayscale difference or an overlap of the grayscale pixel values, it becomes very difficult to get accurate segments. |
|Edge Detection Segmentation |Makes use of discontinuous local features of an image to detect edges and hence define a boundary of the object. | It is good for images having better contrast between objects.|Not suitable when there are too many edges in the image and if there is less contrast between objects. |
|Segmentation based on Clustering |Divides the pixels of the image into homogeneous clusters.|Works really well on small datasets and generates excellent clusters.|Computation time is too large and expensive; k-means is a distance-based algorithm. It is not suitable for clustering non-convex clusters.|
|Mask R-CNN|Gives three outputs for each object in the image: its class, bounding box coordinates, and object mask| Simple, flexible and general approach; It is also the current state-of-the-art for image segmentation|High training time|
#### 1.2.4. Solution
- Instance Segmentation with Mask-RCNN:
- Detection of graphical objects like tables, figures, natural image, logo, etc. is basically localization  of these objects within a document image. The problem is conceptually similar to the detection of objects in natural  scene images. Natural scene objects have visually identifiable characteristics, those features can be extracted by using CNN. Similarly graphical objects can also be identified by the specific characteristics of them, which CNN is capable of. The compelling performance of  COCO  dataset makes us adapting the framework into our graphical object detection  work. 

- Training of deep networks requires a large amount of data. Hence, the IIIT-AR-13K dataset  is the best choice for this work.
The IIIT-AR-13K  is a new dataset for detecting graphical objects in business documents, specifically annual reports. It consists of 13k pages of annual reports of more than ten years of twenty-nine different companies with bounding box annotation of five different object categories | table, figure, natural image, logo, and signature.  
> The newly generated dataset is the largest manually annotated dataset for  graphical object detection purpose, at this stage.  
> 
> This dataset has more labels and diversity compared to most of the existing datasets.  
>
>The iiit-ar-13k is smaller than the existing automatic annotated datasets | deepfigures, publaynet, and tablenet, the model trained with  iiit-ar-13k performs better than the model trained with the larger datasets for detecting tables in document images in most cases.  
>
>Models trained with the existing datasets also achieve better performance  by fine-tuning with a limited number of training images from iiit-ar-13k.

## 2. Image Search Engines
### 2.1. Purpose
- Building a image search engines, that given us dataset of graphical objects in document images, we want to make this dataset "search-able" by creating a “more like this” functionality — this will be a “search by example” image search engine.
### 2.2. Introduction
- Image search engines that quantify the contents of an image are called **Content-Based Image Retrieval** (CBIR) systems. The term CBIR is commonly used in the academic literature, but in reality, it’s simply a fancier way of saying “image search engine”, with the added poignancy that the search engine is relying _strictly_ on the contents of the image and not any textual annotations associated with the image.

![enter image description here](https://pyimagesearch.com/wp-content/uploads/2014/11/tineye_example.jpg)	

A great example of a Search by Example system is [TinEye](https://www.tineye.com/). TinEye is actually a reverse image search engine where you provide a query image, and then TinEye returns near-identical matches of the same image, along with the webpage that the original image appeared on.
#### 2.1.1. Some Important Terms
-  **_index our dataset_**. Indexing a dataset is the process of quantifying our dataset by utilizing an  **_image descriptor_** to extract  **_features_** from each image.

- An  **_image descriptor_** defines the algorithm that we are utilizing to describe our image.

For example:
-   The mean and standard deviation of each Red, Green, and Blue channel, respectively,
-   The statistical moments of the image to characterize shape.
-   The gradient magnitude and orientation to describe both shape and texture.

=> The  **_image descriptor_** governs  **_how_** the image is quantified.

- **_Features_**, on the other hand, are the output of an  **_image descriptor_**. When we put an image into an image descriptor, you will get  **_features_** out the other end.

- In the most basic terms,  **_features_** (or  **_feature vectors_**) are just a list of numbers used to abstractly represent and quantify images.

Take a look at the example figure below:

[![Figure 4: The pipeline of an image descriptor. An input image is presented to the descriptor, the image descriptor is applied, and a feature vector (i.e a list of numbers) is returned, used to quantify the contents of the image.](https://pyimagesearch.com/wp-content/uploads/2014/11/describing_images.jpg)](https://pyimagesearch.com/wp-content/uploads/2014/11/describing_images.jpg)

**Figure :**  The pipeline of an image descriptor. An input image is presented to the descriptor, the image descriptor is applied, and a feature vector (i.e a list of numbers) is returned, used to quantify the contents of the image.

Here we are presented with an input image, we apply our image descriptor, and then our output is a list of features used to quantify the image.

Feature vectors can then be compared for similarity by using a  **_distance metric_** or  **_similarity function_**. Distance metrics and similarity functions take two feature vectors as inputs and then output a number that represents how “similar” the two feature vectors are.

The figure below visualizes the process of comparing two images:

[![Figure : To compare two images, we input the respective feature vectors into a distance metric/similarity function. The output is a value used to represent and quantify how "similar" the two images are to each other..](https://pyimagesearch.com/wp-content/uploads/2014/11/comparing_images.jpg)](https://pyimagesearch.com/wp-content/uploads/2014/11/comparing_images.jpg)

**Figure:**  To compare two images, we input the respective feature vectors into a distance metric/similarity function. The output is a value used to represent and quantify how “similar” the two images are to each other..

- Given two feature vectors, a distance function is used to determine how similar the two feature vectors are. The output of the distance function is a single floating point value used to represent the similarity between the two images.

#### 2.1.2. The 4 Steps of Any CBIR System

No matter what Content-Based Image Retrieval System we are building, they all can be boiled down into 4 distinct steps:

1.  **Defining your image descriptor:**  At this phase we need to decide what aspect of the image we want to describe. Are we interested in the color of the image? The shape of an object in the image? Or do we want to characterize texture?
2.  **Indexing your dataset:** Now that we have our image descriptor defined, our job is to apply this image descriptor to each image in your dataset, extract features from these images, and write the features to storage (ex. CSV file, RDBMS, Redis, etc.) so that they can be later compared for similarity.
3.  **Defining your similarity metric:** We have a bunch of feature vectors. But how are we going to compare them? Popular choices include the Euclidean distance, Cosine distance, and chi-squared distance, but the actual choice is highly dependent on (1) our dataset and (2) the types of features you extracted.
4.  **Searching:** The final step is to perform an actual search. A user will submit a query image to our system (from an upload form or via a mobile app, for instance) and our job will be to (1) extract features from this query image and then (2) apply our similarity function to compare the query features to the features already indexed. From there, we simply return the most relevant results according to our similarity function.





























