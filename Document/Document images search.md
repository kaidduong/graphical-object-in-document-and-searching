﻿
### **Project**: Tìm kiếm tài liệu thông qua hình ảnh

**I.** **Introduction:**

Ngày nay, với sự phát triển của công nghệ số, sự phổ biến của các thiết bị di động và các dịch vu dựa trên đám mây, nhu cầu vê các phương pháp tiếp cận vừa nhanh vừa chính xác trong việc tìm kiếm dữ liệu về các tài liệu số cũng đang cấp thiết . Việc chúng ta bất ngờ bắt gặp một hình ảnh và ngay đó muốn thông qua nó, tìm kiếm chính xác, nhanh chóng các hình ảnh liên quan đến hình ảnh đó cũng là một trường hợp rất phổ biến và quan trọng.

**II.** **Purpose:**

Phát triển một hệ thống để tìm kiếm nhanh các hình ảnh liên quan trong tài liệu một cách nhanh chóng và chính xác thông qua hình ảnh cho trước.

**III.** **Methodology:**
**1. Layout**: 
> Method : Instance segmentation
> Data:  IIIT-AR-13K
> Baseline model: Mask R-CNN ( regional convolutional neural network)

**2. Image search engine:**
- The 4 Steps of CBIR System:
 > 1. Define a image descriptor
>2.  Indexing the dataset
> 3. Define the similarity metric
 > 4. Perfom a search , rank the images in the index in terms of relevancy to user, and display the results to the user.
 
**3. Web-app system**: Building a website system by using some technology: Flask, Blue-print, Posgress Database.

**IV.** Implimentation Details
1. Layout
1.1 IIIT-AR-13K Data
- This dataset, IIIT-AR-13K, is created by manually annotating the bounding boxes of graphical or page objects in publicly available annual reports. This dataset contains a total of 13K annotated page images with objects in five different popular categories — table, figure, natural image, logo, and signature.
![enter image description here](http://cvit.iiit.ac.in/usodi/img/projects/detection/iiit-ar-13/iiit-ar-13k-table1.png)
**Statistics of the IIIT-AR-13K dataset.**

![enter image description here](http://cvit.iiit.ac.in/usodi/img/projects/detection/iiit-ar-13/detection-fig01.png)
Sample annotated document images of IIIT-AR-13K dataset.  **Dark Green:**  indicates ground truth bounding box of table,  **Dark Red:**  indicates ground truth bounding box of figure,  **Dark Blue:**  indicates ground truth bound- ing box of natural image,  **Dark Yellow:**  indicates ground truth bounding box of logo and  **Dark Pink:** indicates ground truth bounding box of signature.

1.2. Mask R-CNN
- Mask R-CNN (regional convolutional neural network) is a two stage framework: the first stage scans the image and generates proposals (areas likely to contain an object). And the second stage classifies the proposals and generates bounding boxes and masks.
- It was introduced last year via the [Mask R-CNN paper](https://arxiv.org/abs/1703.06870) to extend its predecessor, [Faster R-CNN](https://arxiv.org/abs/1506.01497), by the same authors. Faster R-CNN is a popular framework for object detection, and Mask R-CNN extends it with instance segmentation, among other things.
![Mask R-CNN framework. Source: https://arxiv.org/abs/1703.06870](https://miro.medium.com/max/700/1*IWWOPIYLqqF9i_gXPmBk3g.png)
Mask R-CNN framework. Source: [https://arxiv.org/abs/1703.06870](https://arxiv.org/abs/1703.06870)
- At a high level, Mask R-CNN consists of these modules:
**Module 1:** Backbone
> This is a standard convolutional neural network (typically, ResNet50 or ResNet101) that serves as a feature extractor. The early layers detect low level features (edges and corners), and later layers successively detect higher level features (car, person, sky).
>
>Passing through the backbone network, the image is converted from 1024x1024px x 3 (RGB) to a feature map of shape 32x32x2048. This feature map becomes the input for the following stages.
>![enter image description here](https://miro.medium.com/max/309/1*IDjLXsSw5QMFWDudayIBfw.png)
>Simplified illustration of the backbone nework

> ** Feature pyramid network **
>> While the backbone described above works great, it can be improved upon. The  [Feature Pyramid Network (FPN)](https://arxiv.org/abs/1612.03144)  was introduced by the same authors of Mask R-CNN as an extension that can better represent objects at multiple scales.
>>
>> FPN improves the standard feature extraction pyramid by adding a second pyramid that takes the high level features from the first pyramid and passes them down to lower layers. By doing so, it allows features at every level to have access to both, lower and higher level features.
>>![enter image description here](https://miro.medium.com/max/452/1*1sCveJrqfthOQsGGZRs2tQ.png)
>>Source: Feature Pyramid Networks paper

**Module 2:** Region Proposal Network (RPN)
> The RPN is a lightweight neural network that scans the image in a sliding-window fashion and finds areas that contain objects.
>
> The regions that the RPN scans over are called  _anchors_. Which are boxes distributed over the image area, as show on the left. This is a simplified view, though. In practice, there are about 200K anchors of different sizes and aspect ratios, and they overlap to cover as much of the image as possible.
> ![enter image description here](https://miro.medium.com/max/500/1*ESpJx0XLvyBa86TNo2BfLQ.png)
> **Simplified illustration showing 49 anchor boxes**


> How fast can the RPN scan that many anchors? Pretty fast, actually. The sliding window is handled by the convolutional nature of the RPN, which allows it to scan all regions in parallel (on a GPU). Further, the RPN doesn’t scan over the image directly (even though we draw the anchors on the image for illustration).  Instead, the RPN scans over the backbone feature map.  This allows the RPN to reuse the extracted features efficiently and avoid duplicate calculations. With these optimizations, the RPN runs in about 10 ms according to the  [Faster RCNN paper](https://arxiv.org/abs/1506.01497)  that introduced it. In Mask RCNN we typically use larger images and more anchors, so it might take a bit longer.
> >>
> The RPN generates two outputs for each anchor:
>>![enter image description here](https://miro.medium.com/max/407/1*EMNE8bxOT4RI3HMjIqjCwQ.png)
>>**3 anchor boxes (dotted) and the shift/scale applied to them to fit the object precisely (solid). Several anchors can map to the same object**
>>
>> 1.  **Anchor Class:**  One of two classes: foreground or background. The FG class implies that there is likely an object in that box.
>> 2.  **Bounding Box Refinement:**  A foreground anchor (also called positive anchor) might not be centered perfectly over the object. So the RPN estimates a delta (% change in x, y, width, height) to refine the anchor box to fit the object better.
>
>Using the RPN predictions, we pick the top anchors that are likely to contain objects and refine their location and size. If several anchors overlap too much, we keep the one with the highest foreground score and discard the rest (referred to as Non-max Suppression). After that we have the final _proposals_ (regions of interest)  that we pass to the next stage.
>
**Module 3:** ROI Classifier & Bounding Box Regressor
> This stage runs on the regions of interest (ROIs) proposed by the RPN. And just like the RPN, it generates two outputs for each ROI:
![enter image description here](https://miro.medium.com/max/700/1*xQYuM_9mu5kt8nNN8Ms2TQ.png)
>Illustration of stage 2. Source: Fast R-CNN ([https://arxiv.org/abs/1504.08083](https://arxiv.org/abs/1504.08083))
>
>1.  **Class:**  The class of the object in the ROI. Unlike the RPN, which has two classes (FG/BG), this network is deeper and has the capacity to classify regions to specific classes (person, car, chair, …etc.). It can also generate a  background  class, which causes the ROI to be discarded
> 2.  **Bounding Box Refinement:**  Very similar to how it’s done in the RPN, and its purpose is to further refine the location and size of the bounding box to encapsulate the object.
>
>**ROI Pooling**
>>There is a bit of a problem to solve before we continue. Classifiers don’t handle variable input size very well. They typically require a fixed input size. But, due to the bounding box refinement step in the RPN, the ROI boxes can have different sizes. That’s where ROI Pooling comes into play.
>> ![enter image description here](https://miro.medium.com/max/645/1*bsT00ickNk7vaRJNrTvKPQ.png)
>> The feature map here is from a low-level layer, for illustration, to make it easier to understand.
>>
>>The authors of Mask R-CNN suggest a method they named ROIAlign, in which they sample the feature map at different points and apply a bilinear interpolation.

**Module 4:** Segmentation Masks


![Image for post](https://miro.medium.com/max/455/1*l55WzUq1ZD2b5EGwW05LDA.png)

>The mask branch is a convolutional network that takes the positive regions selected by the ROI classifier and generates masks for them. The generated masks are low resolution: 28x28 pixels. But they are  _soft_  masks, represented by float numbers, so they hold more details than binary masks. The small mask size helps keep the mask branch light. During training, we scale down the ground-truth masks to 28x28 to compute the loss, and during inferencing we scale up the predicted masks to the size of the ROI bounding box and that gives us the final masks, one per object.

2. Image search engine

